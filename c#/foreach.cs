using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace TestForeach
{
    internal class Program
    {
        //上次摘录了 我们队伍中8年工作经验的、怼天怼地怼空气的西南交大女硕士的非空校验的代码。
        //再来看一段她老人家的大手笔，绝对也是千万级价值的代码
        static void Main(string[] args)
        {
            DataDao dao = new DataDao();//假设是数据层
            List<Data> datas = dao.GetDatas();//从数据库获取数据

            DataTable table = new DataTable();//这个table已经有一万多条数据了，数据结构和 Data 类一样


            List<object[]> listRows = new List<object[]>();


            //目的：检查table中是否存在 datas的数据，如果没有就加入，如果有就跳过


            //价值千万的魔鬼循环来了
            foreach (Data data in datas)
            {
                foreach (DataRow row in table.Rows)
                {
                    int id = Convert.ToInt32(row[0]);
                    int count = datas.Where(p => p.Id == id).Count();//注意这一句
                    if (count == 0)
                    {
                        //我的内心：哟，还不错，不愧是8年工作经验的女硕士，
                        //        还知道这里不能直接 table.Rows.Add(new object[] { data.Id, data.Filed1, data.Filed2, data.Filed3 });
                        //        知道这么干会报错。
                        object[] newRow = new object[] { data.Id, data.Filed1, data.Filed2, data.Filed3 };
                        listRows.Add(newRow);
                    }
                }
            }


            //再把判定为不存在的数据加到 DataTable中。
            foreach (object[] row in listRows)
            {
                table.Rows.Add(row);
            }
            //最后导出到报表 就忽略了。


            //测试组的同事说：xxx硕士，你这个功能怎么导出数据要十几分钟啊。
            //女硕士说：没法儿解决，这已经是最快的方式了。（真他妈自信）
            //组长受不了了，让我给女硕士处理。
            //我一看，说：你西南交大的教授这么教你写这么辣眼睛的循环的？
            //          先不说你这个屎一样的循环，外层循环遍历十几万次，二层循环又要遍历几万次，三层循环又要遍历十几万次。
            //女硕士怼我：什么三层循环啊？我只用了两层。
            //我怒了：你他妈 datas.Where(p => p.Id == id).Count() 这不是循环是什么鬼。
            //女硕士：那就一句代码，怎么就循环了。（好一个“就一句代码，怎么就循环了”）
            //我私聊组长：组长，我无能为力，要么让她自己倒腾，要么让她闭嘴。
            //组长：那我让她闭嘴，你改。
        }
    }

    class Data
    {
        public int Id { get; set; }
        public string Filed1 { get; set; }
        public string Filed2 { get; set; }
        public string Filed3 { get; set; }
    }

    //数据层
    class DataDao
    {
        public List<Data> GetDatas()
        {
            //从数据库获取数据
            return new List<Data>();
        }
    }
}
