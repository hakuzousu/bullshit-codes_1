package com.vellnice.guidelamp.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


public class MultiTableHelper extends SQLiteOpenHelper {

    // ......
    // 无关代码省略
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        if (db != null) {
            if (tableNames != null && sqls != null) {
                for (int i = 0; i < tableNames.length; i++) {
                    db.execSQL("create table if not exists " + tableNames[i] + sqls[i]);
                }
            }
        }
    }

    // 数据库一旦升级就把所有数据表删除重新创建
    // 以下注释是原开发者的注释，他很清楚自己在干嘛...

    // 版本更新
    // 用于升级数据库，当Version 变动了，就会调用onUpgrade方法
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (db != null) {
            if (tableNames != null) {
                // 如果表存在就删除
                for (int i = 0; i < tableNames.length; i++) {
                    db.execSQL("drop table if exists" + tableNames[i]);
                }
                // 重新初始化
                onCreate(db);
            }
        }
    }


}
