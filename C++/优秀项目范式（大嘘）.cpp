// sample.h - declaration
#include <bits/stdc++.h>
namespace MATH {
	struct Math {
		int gcd(int x, int y);
		int divide(int a, int b, int& remainder);
		double avg(double *a, int n);
		struct Point {
			double x, y;
		};
		double distance(Point *p1, Point *p2);
		bool in_mandel(double x, double y, int n);
	};
}
#if __cplusplus >= 201103L
namespace MATH {
	bool is_circle(int x, int y) noexcept;
	namespace MATH_literals {
		bool sxhs(int x) noexcept;
	}
}
#else
namespace MATH {
	bool is_circle(int x, int y) throw();
	bool sxhs(int x) throw();
}
#endif

// sample.tcc - implementation
// #include "sample.h"
namespace MATH {
	int Math::gcd(int x, int y) {
		int g = y;
		while (x > 0) {
			g = x;
			x = y % x;
			y = g;
		}
		return g;
	}
	int Math::divide(int a, int b, int& remainder) {
		int quot = a / b;
		remainder = a % b;
		return quot;
	}
	double Math::avg(double *a, int n) {
		int i;
		double total = 0.0;
		for (i = 0; i < n; i++) {
			total += a[i];
		}
		return total / n;
	}
	double square(double x) {
		return x * x;
	}
	double cube(double x) {
		return square(x) * x;
	}
	double Math::distance(Point *p1, Point *p2) {
		return sqrt(square(p1->x - p2->x) + square(p1->y - p2->y));
	}
	bool Math::in_mandel(double x, double y, int n) {
		double x0 = 0, y0 = 0, xtemp;
		while (n > 0) {
			xtemp = square(x0) - square(y0) + x;
			y0 = 2 * x0 * y0 + y;
			x0 = xtemp;
			n -= 1;
			if (square(x0) + square(y0) > 4) {
				return false;
			}
		}
		return true;
	}
	bool is_circle(int x, int y)
	#if __cplusplus >= 201103L
	noexcept
	#else
	throw()
	#endif
	{
		return square(x) + square(y) == 1;
	}
	#if __cplusplus >= 201103L
	namespace MATH_literals {
	#endif
	bool sxhs(int x)
	#if __cplusplus >= 201103L
	noexcept
	#else
	throw()
	#endif
	{
		if (x < 100 && x > 999)
			return false;
		else if (x % 100 == 0)
			return true;
		int x1 = x / 100;
		int x2 = (x - x1 * 100) / 10;
		int x3 = x % 10;
		if (cube(x1) + cube(x2) + cube(x3) == x) {
			return true;
		}
		return false;
	}
	#if __cplusplus >= 201103L
	}
	#endif
}

// new_cmath.h - ???
// #include "sample.tcc"
namespace cmath_new = MATH;
#if __cplusplus >= 201103L
namespace cmath_new_ml = cmath_new::MATH_literals;
#endif
typedef cmath_new::Math  cmath_new_cmath;
typedef cmath_new::Math::Point cmath_new_vector;

// test.cpp - how to use
// #include "new_cmath.h"
int main()
{
	using namespace cmath_new;
	cmath_new_cmath m; // instance? idk
	std::cout << m.gcd(3, 5) << '\n';
	return 0;
}

// note:上述代码可以编译运行