#include <iostream>
using namespace std;

/*
 *  根据 decltype(i) 推断类型，返回类型为 int
 */
decltype(auto)
fn_1(int i) {
    return i;
}

/*
 * 根据 decltype((i)) 推断类型，由于(i)是左值，返回类型为 int&
 * 从而产生局部变量的引用
 */
decltype(auto)
fn_2(int i) {
    return (i);
}

int main() {
    cout << fn_1(1) << endl ;       // OK
    cout << fn_2(1) << endl ;       // Segmentation fault
}
